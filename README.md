# Netbox

A script to query a network device, extract the software version, and update a custom field in Netbox. Written for Cisco, Fortigate, Palo Alto, Juniper and Aruba CX devices. Also includes a few unit tests for the regex patterns in the script.

Note about unit tests - the Netmiko module maintainer apparently does not provide unit tests for Netmiko, so I put the regex patterns in a separate file and created tests on those. Also, I have little experience writing unit tests so they're probably not written in the most optimal way.
