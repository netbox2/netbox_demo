#!/usr/bin/env python

"""
Author: David Wing
Purpose: Test regex patterns against the 'show version' output from an Aruba switch
"""

import unittest
from regex_functions import return_arubaos_version

class ArubaUnitTest(unittest.TestCase):
    """Class object for Aruba unit tests"""
    def test_parse_aruba_version(self):
        """Test regex pattern against expected output"""
        aruba_model_output = """
            -----------------------------------------------------------------------------
            ArubaOS-CX
            (c) Copyright 2017-2020 Hewlett Packard Enterprise Development LP
            -----------------------------------------------------------------------------
            Version      : TL.xx.xx.xxxx
            Build Date   : 2020-08-20 10:56:02 PDT
            Build ID     : ArubaOS-CX:xx.xx.xxxx:feb590a400a5:201908201736
            Build SHA    : feb590a400a57ed818b01614f92010d74fbc9a4b
            Active Image : secondary
            
            Service OS Version : TL.01.03.0008
            BIOS Version       : TL-01-0013
        """
        aruba_model_data = return_arubaos_version(aruba_model_output)
        aruba_value = "TL.01.03.0008"
        self.assertEqual(aruba_model_data, aruba_value)

        aruba_negative_model_output = """
            -----------------------------------------------------------------------------
            ArubaOS-CX
            (c) Copyright 2017-2020 Hewlett Packard Enterprise Development LP
            -----------------------------------------------------------------------------
            Version      : TL.xx.xx.xxxx
            Build Date   : 2020-08-20 10:56:02 PDT
            Build ID     : ArubaOS-CX:xx.xx.xxxx:feb590a400a5:201908201736
            Build SHA    : feb590a400a57ed818b01614f92010d74fbc9a4b
            Active Image : secondary
            
            Service OS Version : TL.01..03..0008
            BIOS Version       : TL-01-0013
        """
        aruba_negative_model_data = return_arubaos_version(aruba_negative_model_output)
        assert aruba_negative_model_data is None


if __name__ == '__main__':
    unittest.main()
