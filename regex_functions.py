#!/usr/bin/env python

"""
Author: David Wing
Purpose: Provides functions to unit test scripts
"""
import re

def return_ios_version(model_output):
    """Regex pattern for IOS output"""
    ios_regex_match = re.compile(r"(\d\d\.\d*)")
    ios_version = ios_regex_match.search(model_output)
    if ios_version:
        return ios_version.group()
    return None

def return_asa_version(model_output):
    """Regex pattern for ASA output"""
    asa_regex_match = re.compile(r"\d\.\d\d\(\d\)")
    asa_version = asa_regex_match.search(model_output)
    if asa_version:
        return asa_version.group()
    return None

def return_fortigate_version(model_output):
    """Regex pattern for Fortigate output"""
    regex_match = re.compile(r"\d\.\d\.\d\d")
    fortigate_version = regex_match.search(model_output)
    if fortigate_version:
        return fortigate_version.group()
    return None

def return_arubaos_version(model_output):
    """Regex pattern for Aruba CX output"""
    regex_match = re.compile(r"([A-Z][A-Z]\.\d\d\.\d\d\.\d*)")
    arubaos_verion = regex_match.search(model_output)
    if arubaos_verion:
        return arubaos_verion.group()
    return None

def return_panos_version(model_output):
    """Regex pattern for Palo Alto output"""
    regex_match = re.compile(r"\d\.\d\.\d")
    panos_version = regex_match.search(model_output)
    if panos_version:
        return panos_version.group()
    return None

def return_junos_version(model_output):
    """Regex pattern for Juniper output"""
    regex_match = re.compile(r"(\d\d\.\d\w\d.\d\d)")
    junos_version = regex_match.search(model_output)
    if junos_version:
        return junos_version.group()
    return None
