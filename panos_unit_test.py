#!/usr/bin/env python

"""
Author: David Wing
Purpose: Test regex patterns against the 'show system info' output from a Palo Alto firewall
"""

import unittest
from regex_functions import return_panos_version

class PANOSUnitTest(unittest.TestCase):
    """Class object for PAN OS unit tests"""
    def test_parse_panos_version(self):
        """Test regex pattern against expected output"""
        panos_model_output = """
            hostname: PA-850
            ip-address: 10.10.10.23
            public-ip-address: unknown
            netmask: 255.255.255.0
            default-gateway: 10.10.10.1
            ip-assignment: static
            ipv6-address: unknown
            ipv6-link-local-address: fe80::d6f4:beff:febe:ba00/64
            ipv6-default-gateway:
            mac-address: d4:f4:be:be:ba:00
            time: Tue May  29 08:40:09 2023
            uptime: 6 days, 11:51:18
            family: 800
            model: PA-850
            serial: unknown
            cloud-mode: non-cloud
            sw-version: 8.0.0
        """
        panos_model_data = return_panos_version(panos_model_output)
        panos_value = "8.0.0"
        self.assertEqual(panos_model_data, panos_value)

        panos_negative_model_output = """
            hostname: PA-850
            ip-address: 10.10.10.23
            public-ip-address: unknown
            netmask: 255.255.255.0
            default-gateway: 10.10.10.1
            ip-assignment: static
            ipv6-address: unknown
            ipv6-link-local-address: fe80::d6f4:beff:febe:ba00/64
            ipv6-default-gateway:
            mac-address: d4:f4:be:be:ba:00
            time: Tue May  29 08:40:09 2023
            uptime: 6 days, 11:51:18
            family: 800
            model: PA-850
            serial: unknown
            cloud-mode: non-cloud
            sw-version: 8.0...0
        """
        panos_negative_model_data = return_panos_version(panos_negative_model_output)
        assert panos_negative_model_data is None

if __name__ == '__main__':
    unittest.main()
