#!/usr/bin/env python

"""
Author: David Wing
Puurpose: Unit test for regex patterns against the 'show version' output from a Juniper device
"""

import unittest
from regex_functions import return_junos_version


class JunOSUnitTest(unittest.TestCase):
    """Class object for JunOS unit tests"""
    def test_parse_junos_version(self):
        """Tests regex pattern again expected output"""
        junos_model_output = """
            Hostname: jtac-QFX5120
            Model: qfx5120-48y-8c
            Junos: 22.2R2.10
        """
        junos_model_data = return_junos_version(junos_model_output)
        junos_version = "22.R2.10"
        self.assertEqual(junos_model_data, junos_version)

        junos_negative_model_output = """
            Hostname: jtac-QFX5120
            Model: qfx5120-48y-8c
            Junos: 22.2R..10
        """
        junos_negative_model_data = return_junos_version(junos_negative_model_output)
        assert junos_negative_model_data is None

if __name__ == '__main__':
    unittest.main()
