#!/usr/bin/env python
"""
Author: David Wing
Purpose: Extract software version from network equipment and update custom field in Netbox
"""

import getpass
import re
import pynetbox
import netmiko


def main():
    """Run functions to get device details and update Netbox"""
    device = get_device_parameters()

    if "aruba_os" in device.values():
        software_version = get_aruba_software_version(device)
    elif "cisco_ios" in device.values():
        software_version = get_ios_software_version(device)
    elif "cisco_asa" in device.values():
        software_version = get_ios_software_version(device)
    elif "fortinet" in device.values():
        software_version = get_fortigate_software_version(device)
    elif "juniper_junos" in device.values():
        software_version = get_juniper_software_version(device)
    elif "paloalto_panos" in device.values():
        software_version = get_palo_software_version(device)

    update_netbox_device(software_version)


def get_device_parameters():
    """Creates dictionary object containing necessary parameters needed by netmiko"""
    device_list = input("[*] Enter the hostname or IP address of the remote device: ")
    username = input("[*] Enter the username: ")
    password = getpass.getpass("[*] Password: ")
    dev_type = input(
        "[*] Enter the device type - the available types are: \
        \n\t'aruba_os'\n\t'cisco_asa'\n\t'cisco_ios'\n\t' \
        fortinet'\n\t'juniper_junos'\n\t'paloalto_panos'\n>"
    )

    device_params = {
        "device_type": dev_type.lower(),
        "host": device_list,
        "username": username,
        "password": password,
    }

    return device_params


def get_ios_software_version(device):
    """Connects to an IOS or ASA device and returns the software version number."""
    with netmiko.ConnectHandler(**device) as connection:
        show_ver_output = connection.send_command("show version")

    if device.get("device_type") == "cisco_ios":
        regex_match = re.compile(r"\d\d\.\d*")
    elif device.get("device_type") == "cisco_asa":
        regex_match = re.compile(r"\d\.\d\d\(\d\)")
    ios_version = regex_match.search(show_ver_output)

    print(f"[*] IOS version appears to be {ios_version.group()}")
    return ios_version.group()


def get_aruba_software_version(device):
    """Connects to an Aruba OS device and returns the software version number."""
    with netmiko.ConnectHandler(**device) as connection:
        show_ver_output = connection.send_command("show version")

    regex_match = re.compile(r"([A-Z][A-Z]\.\d\d\.\d\d\.\d*)")
    arubaos_verion = regex_match.search(show_ver_output)

    print(f"[*] The Aruba OS version appears to be {arubaos_verion.group()}")
    return arubaos_verion.group()


def get_palo_software_version(device):
    """Connects to a Palo Alto device and returns the software version number."""
    with netmiko.ConnectHandler(**device) as connection:
        get_sys_output = connection.send_command("show system info")

    regex_match = re.compile(r"\d\.\d\.\d")
    panos_version = regex_match.search(get_sys_output)

    print(f"[*] The PAN OS version appears to be {panos_version.group()}")
    return panos_version.group()


def get_juniper_software_version(device):
    """Connects to a Juniper device and returns the software version number."""
    with netmiko.ConnectHandler(**device) as connection:
        get_sys_output = connection.send_command("show version")

    regex_match = re.compile(r"(\d\d\.\d\w\d.\d\d)")
    junos_version = regex_match.search(get_sys_output)

    print(f"[*] The JUNOS version appears to be {junos_version.group()}")
    return junos_version.group()


def get_fortigate_software_version(device):
    """Connects to a Fortigate device and returns the software version number"""
    with netmiko.ConnectHandler(**device) as connection:
        get_sys_output = connection.send_command("get system status")

    regex_match = re.compile(r"\d\.\d\.\d\d")
    fortigate_version = regex_match.search(get_sys_output)

    print(f"[*] The Fortigate version appears to be {fortigate_version.group()}")
    return fortigate_version.group()


def update_netbox_device(software_version):
    """Updates custom field 'software version' in Netbox with software version value of device."""
    api_host = input("[*] Enter the IP or hostname of the Netbox API: ")
    api_token = getpass.getpass("[*] Enter your API token: ")
    device_name = input("[*] Enter the device name in Netbox to be updated: ")

    netbox_api = pynetbox.api(f"http://{api_host}:8000", token=api_token)

    try:
        target_device = netbox_api.dcim.devices.get(
            tenant="noc", status="active", name=device_name
        )
        print(
            f"[*] Current value of software version field: {target_device.custom_fields}"
        )
        target_device.custom_fields = {"sw_version": software_version}
        target_device.save()
        print(
            f"[*] New value of software version field after update: \
            {target_device.custom_fields}"
        )
    except AttributeError:
        print("[*] Device doesn't appear to exist in Netbox.")


if __name__ == "__main__":
    main()
