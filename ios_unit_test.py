#!/usr/bin/env python

"""
Author: David Wing
Purpose: Test regex patterns against the 'show version' output from Cisco devices
"""

import unittest
from regex_functions import return_ios_version, return_asa_version

class IOSUnitTest(unittest.TestCase):
    """Class object for IOS unit testing"""
    def test_parse_ios_version(self):
        """Tests regex pattern against expected output"""
        ios_model_output = """
            Cisco IOS Software, vios_l2 Software (vios_l2-ADVENTERPRISEK9-M), Experimental Version 15.2(20200924:215240) [sweickge-sep24-2020-l2iol-release 135]
            Copyright (c) 1986-2020 by Cisco Systems, Inc.
            Compiled Tue 29-Sep-20 11:53 by sweickge
        """
        ios_model_data = return_ios_version(ios_model_output)
        ios_value = "15.2"
        self.assertEqual(ios_model_data, ios_value)

        negative_ios_model_output = """
            Cisco IOS Software, vios_l2 Software (vios_l2-ADVENTERPRISEK9-M), Experimental Version (20200924:215240) [sweickge-sep24-2020-l2iol-release 135]
            Copyright (c) 1986-2020 by Cisco Systems, Inc.
            Compiled Tue 29-Sep-20 11:53 by sweickge
        """
        negative_ios_model_data = return_ios_version(negative_ios_model_output)
        print(negative_ios_model_data)
        assert negative_ios_model_data is None

        asa_model_ouput = """
                Cisco Adaptive Security Appliance Software Version 9.16(2) 
                SSP Operating System Version 2.10(1.162)
                Device Manager Version 7.16(1)
            """
        asa_model_data = return_asa_version(asa_model_ouput)
        asa_value = "9.16(2)"
        self.assertEqual(asa_model_data, asa_value)

        negative_asa_model_output = """
            Cisco Adaptive Security Appliance Software Version 9.16(2 
            SSP Operating System Version 2.10(1.162)
            Device Manager Version 7.16(1)
        """
        negative_asa_model_data = return_asa_version(negative_asa_model_output)
        print(negative_asa_model_data)
        assert negative_ios_model_data is None

if __name__ == "__main__":
    unittest.main()
