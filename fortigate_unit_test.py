#!/usr/bin/env python

"""
Author: David Wing
Purpose: Unit test for regex pattern against the 'get system status' output from a Fortigate firewall.
"""

import unittest
from regex_functions import return_fortigate_version

class FortigateUnitTest(unittest.TestCase):
    """Class object for Fortigate unit tests"""
    def test_parse_fortigate_version(self):
        """Tests regex pattern against expected output"""
        fortigate_model_output = """
            Version: FortiFirewall-VM64-KVM v7.0.11,build0489,230314 (GA.M)
            Serial-Number: FFVMEVBNJLGN8O0E
            License Status: Valid
            Evaluation License Expires: Thu Jun 15 05:05:42 2023
            VM Resources: 1 CPU/1 allowed, 997 MB RAM/2048 MB allowed
            Log hard disk: Not available
            Hostname: FortiFirewall-VM64-KVM
            Operation Mode: NAT
            Current virtual domain: root
            Max number of virtual domains: 1
            Virtual domains status: 1 in NAT mode, 0 in TP mode
            Virtual domain configuration: disable
            FIPS-CC mode: disable
            Current HA mode: standalone
            Branch point: 0489
            Release Version Information: GA
            FortiOS x86-64: Yes
            System time: Mon Jun  5 01:00:14 2023
            Last reboot reason: power cycle
        """
        fortigate_model_data = return_fortigate_version(fortigate_model_output)
        fortigate_value = "7.0.11"
        self.assertEqual(fortigate_model_data, fortigate_value)

        fortigate_negative_model_output = """
            Version: FortiFirewall-VM64-KVM v7.0..1,build0489,230314 (GA.M)
            Serial-Number: FFVMEVBNJLGN8O0E
            License Status: Valid
            Evaluation License Expires: Thu Jun 15 05:05:42 2023
            VM Resources: 1 CPU/1 allowed, 997 MB RAM/2048 MB allowed
            Log hard disk: Not available
            Hostname: FortiFirewall-VM64-KVM
            Operation Mode: NAT
            Current virtual domain: root
            Max number of virtual domains: 1
            Virtual domains status: 1 in NAT mode, 0 in TP mode
            Virtual domain configuration: disable
            FIPS-CC mode: disable
            Current HA mode: standalone
            Branch point: 0489
            Release Version Information: GA
            FortiOS x86-64: Yes
            System time: Mon Jun  5 01:00:14 2023
            Last reboot reason: power cycle
        """
        fortigate_negative_model_data = return_fortigate_version(fortigate_negative_model_output)
        print(fortigate_negative_model_data)
        assert fortigate_negative_model_data is None

if __name__ == "__main__":
    unittest.main()
